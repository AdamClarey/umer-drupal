<section class="chat-window" id="chat">
  <div id="responsive-heads">
    <h3 class="recent" data-panel="groupbar"><?php print t('Recent'); ?></h3>
    <h3 class="search" data-panel="chat-search-pane"><?php print t('Search'); ?></h3>
    <h3 class="chat-head active" data-panel="chat-panel"><?php print t('Chat...'); ?></h3>
  </div>
  <section>
    <div id="lookup-panel">
      <div id='lookup-header'>
        <h3 class="lookup-title recent active"><?php print l('Recent','javascript:void(0)',array('fragment' => '','external'=>true)); ?></h3>
        <h3 class="lookup-title search"><?php print l('Search','javascript:void(0)',array('fragment' => '','external'=>true)); ?></h3>
      </div>
      <div id="chat-search-pane" class="lookup-col-container">
        <form id="chat-search" >
          <label>Search for users</label>
          <input id="chat-search-field" />
        </form>
        <ul id="chat-search-results"></ul>
      </div>
      <div id='groupbar' class="lookup-col-container open">
        <ul id='grouplist' class="lookup-col">
        </ul>
			</div>
		</div>
    <div id="chat-panel" class="active">
			<?php
			  if ($settings_link) {
				  print $settings_link;
					print '<div id="chat-settings"></div>';
			  }
			?>
      <div class="mediacall-window-surround window-surround" style="display: none;">
				<div class="calling"><span class="glyphicon glyphicon-earphone"></span><span><?php print t('Calling'); ?></span></div>
        <div id='mediacall-window'></div>
        <ul id='mediacallbuttons'>
          <li id="hangup" class='group-action group-action-hangup' style="display:none;" title="Hang up"><a href='javascript:void(0)'><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Hang up</a>
          </li>
        </ul>
      </div>

      <div id="conversation-wrapper">
      <div id='group-header'>
        <h2><?php print t('Chat...'); ?></h2>
        <div class="member-count-surround">
          <span id='member-count'></span>
          <ul id='group-members' style="display: none"></ul>
        </div>
        <div class="meeting-window-surround window-surround" style="display: none;">
          <h3>Meeting</h3>
          <div id="meeting-window"></div>
        </div>
      </div>
      <div id='conversation' data-groupid>

      </div>
      <form class='submit-message-from'>
        <input class='message-send' type='submit' value='send' />
        <div class='submit-message-box'>
          <span id="typing"></span>
          <input class='message-textfield' id='messageinput' autocomplete='off' />
        </div>
      </form>

      <ul class='group-actions'>
        <li class='group-action group-action-in-bar group-action-add-user' style="display:none;">
          <a><span class='glyphicon glyphicon-plus' aria-hidden='true'></span><?php print t('Add user'); ?></a>
        </li>
        <li class='group-action group-action-in-bar group-action-rename' style="display:none;">
          <a><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span><?php print t('Rename group'); ?></a>
        </li>
        <li class='group-action group-action-in-bar group-action-leave' style="display:none;">
          <a><span class='glyphicon glyphicon-log-out' aria-hidden='true'></span><?php print t('Leave group'); ?></a>
        </li>
        <li class='group-action group-action-in-bar group-action-send-file' style="display:none;">
          <a><span class="glyphicon glyphicon-file" aria-hidden="true"></span><?php print t('Send file'); ?></a>
        </li>
        <li class='group-action group-action-in-bar group-action-voicecall' style="display:none;">
          <a><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><?php print t('Call'); ?></a>
        </li>
        <li class='group-action group-action-in-bar group-action-videocall' style="display:none;">
          <a><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span><?php print t('Video call'); ?></a>
        </li>
      </ul>

      <div id="fileupload" style="display: none">
        <input type="file" id="files" name="files[]" />
      </div>
			</div>
    </div>
  </section>
</section>
<br />
<!--
<div class="chat-window">
  <?php if (arg(0) != 'chat') : ?>
  <div id="popout-link">
    <a target="_blank" href="javascript:void(0)">Open in popout <i class="glyphicon glyphicon-new-window"></i></a>
  </div>
  <?php endif; ?>
</div>
-->
