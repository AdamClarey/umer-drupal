<?php

//Run sync function on user save and user edit

function hub_chat_user_update($edit, $user, $category){

  user_sync($user);

};

function hub_chat_user_save($user){

  user_sync($user);

};

function hub_chat_user_insert(&$edit, $account, $category){

  user_sync($account);

};

function hub_chat_update_7100() {
    db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('name', 'hub_chat', '=')
    ->execute();
}

//Send user data to node server

function user_sync($user){

$fields = field_info_instances("user","user");

$output = array();

//Get hub notification settings if available

//    $settings = db_query('SELECT settings FROM {hub_notifications} WHERE uid = :uid', array(':uid' => $user->uid))->fetchField();
//
//    if ($settings) {
//		$output['notification_settings'] = unserialize($settings);
//
//	}

//foreach ($fields as $name => $field){
//
//$current = field_get_items('user', $user, $name);
//
//$fielddata = field_view_value('user', $user, $name, $current[0]);
//
//if(isset($fielddata['#markup'])){
//$output[$name] = $fielddata['#markup'];
//}
//
//};

//if(empty($output['field_name_first'])){

    if($user->name){
        $output['field_name_first'] = explode(" ",$user->name)[0];
        $output['field_name_last'] = explode(" ",$user->name)[1];
    }
//};

//$output['mail'] = $user->mail;

$output['uid'] = $user->uid;

if(isset($user->picture)){

  $picture = $user->picture->uri;

  $output['picture'] = file_create_url($picture);
  $output['avatar'] = image_style_url('square_150_150', $picture);

}

$output = json_encode($output);

 $data = array(
    'apikey' => variable_get("hub_chat_apikey"),
    'secretkey' => variable_get("hub_chat_secret"),
    'content' => $output,
  );

  $query = drupal_http_build_query($data);

  $options = array(
    'method' => 'POST',
    'data' => $query,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );

 $return = drupal_http_request(variable_get("hub_chat_address") . '/user/sync', $options);

};


function hub_chat_sync() {
  $form = array();
  $form['count'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#description' => t('Limit of users to send to database. 0 for no limit.'),
    '#size' => 10
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Click here to Start'));
  return $form;
}

function hub_chat_sync_submit($form, $form_state){
  batch_set(hub_chat_build_batch($form_state['input']['count']));
}

function hub_chat_build_batch($limit){

  if($limit == 0){

    $limit = INF;

  };

  $data = array(
    'apikey' => variable_get("hub_chat_apikey"),
    'secretkey' => variable_get("hub_chat_secret"),
    'content' => "all",
  );

  $query = drupal_http_build_query($data);

  $options = array(
    'method' => 'POST',
    'data' => $query,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );

 $return = drupal_http_request(variable_get("hub_chat_address") . '/user/fetchall', $options);

$saved = json_decode($return->data);

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'user');

  $result = $query->execute();

  $users = $result['user'];

  $users = array_chunk($users, 10);

  $count = 0;

  foreach ($users as $usergroup){

    if($count < $limit){

    $operations[] = array('hub_chat_process_data', array($usergroup),$saved);

    $count++;

    }

  }

  $batch = array(
    'title' => t('Syncing users to chat server'),
    'operations' => $operations,
    'finished' => 'hub_chat_build_batch_finished',
    'init_message' => t('Initializing...'),
    'progress_message' => t('Sending data for user @current of @total.'),
    'error_message' => t('Found an error.'),
  );

  return $batch;
}

function hub_chat_build_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of data we processed...
    drupal_set_message("Import complete");
  } else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}


function hub_chat_process_data($usergroup,$saved){

  $uids = array();

  foreach ($usergroup as $user){

    if(!isset($saved[$user->uid]) && $user->uid !== 0){

      $uids[] = $user->uid;

    }

  }

  $users = user_load_multiple($uids);

  foreach ($users as $account){

      user_sync($account);

  }

};
