Drupal.theme.prototype.GenericUserImage = function () {

  var html = '';

  html += "/sites/default/files/styles/user_picture_profile/public/default_profile_image.png";

  return html;

}

Drupal.theme.prototype.GenericGroupImage = function () {

  var html = '';

  html += "/sites/all/themes/hub/img/hub-group.png";

  return html;

}

Drupal.theme.prototype.GenericReadOnlyImage = function () {

  var html = '';

  html += "/sites/all/themes/hub/img/hub-icon.png";

  return html;

}

/**
 * Provide the HTML to create chat messages.
 */
Drupal.theme.prototype.ChatMessage = function (message) {

  var html = '';

  var time = chat.mongotime(message._id);
  var formattedtime = chat.formatTime(time);

  html += '<div class="message" data-messageid="' + message._id + '" data-userid="' + message.userid + '" data-toggle="tooltip" title="' + formattedtime.date + ' ' + formattedtime.time + '">';

  // Pass it off to type template
  if (Drupal.theme.prototype.MessageTemplate[message.type]) {

    html += Drupal.theme.prototype.MessageTemplate[message.type]({
      content: message.content,
      formattedtime: formattedtime,
      username: message.username
    });

  }

  // Allow editing in UI
  if (chat.user.id === message.userid && message.type === "text") {
    html += '<span class="message-tools">';
    html += '<span class="fa fa-pencil editmessage"aria-hidden="true" title="' + Drupal.t('Edit') + '" data-toggle="tooltip"></span>';
    html += '<span class="fa fa-trash editmessage"aria-hidden="true" title="' + Drupal.t('Delete') + '" data-toggle="tooltip"></span>';
    html += '</span>';
  }
  html += '</div>';

  return html;
};

/**
 * Provide the HTML to create popup message notifications
 */
Drupal.theme.prototype.PopupMessage = function (message) {
  var html = '';

  html = '<div class="chat-notification" style="display: none" data-groupid="' + message.groupid + '">';

  html += '<div class="chat-notification-header">';
  if (chat.groups[message.groupid].is121) {
    html += message.username;
    html += "<span class='close-popup glyphicon glyphicon-remove'></span>";
    html += '</div>';
    html += '<div class="chat-notification-sub">';
    html += ' at ' + Date.now().toString();
    html += '</div>';
  } else {
    html += chat.groups[message.groupid].name;
    html += "<span class='close-popup glyphicon glyphicon-remove'></span>";
    html += '</div>';
    html += '<div class="chat-notification-sub">';
    html += message.username + ' at ' + Date.now().toString();
    html += '</div>';
  }
  html += '<div class="chat-notification-message">';
  html += message.content.text;
  html += '</div>';
  html += '</div>';

  return html;

};

Drupal.theme.prototype.GroupListItem = function (group) {
  var html = '';

  var classes = [];
  var grouptype;

  // For 121 groups...
  var userid = '';
  var online = '';

  // Check 121 and add relevant data attributes
  if (group.is121) {
    group.members.forEach(function(element) {
      if (element.userid != chat.user.id) {

        if(jQuery.inArray(element.userid,chat.onlineusers) !== -1){

          online = "online";

        }

        userid = ' data-userid="' + element.userid + '"';
      }
    });
    grouptype = '1to1';
    classes.push('1to1');
  }

  // Check readonly
  if (group.isReadOnly) {
    grouptype = 'readonly';
    classes.push('readonly');
  }

  // Look for reference type
  if (group.reftype) {
    if (group.reftype === 'event') {
      classes.push('event');
    } else if (group.reftype === 'og') {
      classes.push('og');
    }
  }

  if (!group.avatar) {
    if (group.isReadOnly) {
      // Generic Hub group image
      group.avatar = Drupal.theme.prototype.GenericReadOnlyImage();
    } else if (group.is121) {
      // Generic user profile image
      group.avatar = Drupal.theme.prototype.GenericUserImage();
    } else {
      // Image used for ad hoc groups
      group.avatar = Drupal.theme.prototype.GenericGroupImage();
    }
  }

  var printclasses = '';

  jQuery.each(classes, function(index, element) {
    printclasses += element;
    printclasses += ' ';
  });

  html += '<li class="group '+ printclasses + '" title="' + group.name + '" data-groupid="' + group._id + '"' + userid + 'data-grouptype="' + grouptype + '"' + '>';
  html += '  <span class="image-container online-surround '+online+'">';
  html += '    <img src="' + group.avatar + '">';
  html += '  </span>';
  html += '  <span>' + group.name + '</span> <span class="unread">';
  if (group.unread && group.unread > 0) {
    html += group.unread;
  }
  html += '</span>';
  if (group.reftype === 'event') {
    html += '<span class="event-start" data-starttime="' + group.starttime + '" data-endtime="' + group.endtime + '">(' + Drupal.theme.prototype.MeetingTime(group.starttime * 1000, group.endtime * 1000) + ')</span>';
  }
  html += '</li>';

  return html;
};

Drupal.theme.prototype.UserSearchItem = function (user) {
  var html = '';
  var onlineclasses = 'online-surround';

  if (jQuery.inArray(user.uid, chat.onlineusers) > -1) {
    onlineclasses += ' online';
  }

  html += '<li class="user" data-userid="' + user.uid + '">';
  if (user.avatar) {
    html += '  <span class="image-container ' + onlineclasses + '"><img src="' + user.avatar + '"></span>';
  } else {
    html += '  <span class="image-container ' + onlineclasses + '"><img src="' + Drupal.theme.prototype.GenericUserImage() + '"></span>';
  }
  html += '<span class="name">' + user.name + '</span>';
  if (chat.user.activegroup && !chat.groups[chat.user.activegroup].isReadOnly) {
    //Check if user isn't already in group

    var alreadyin = false;
    jQuery.each(chat.groups[chat.user.activegroup].members, function (index, element) {

      if (element.userid === user.uid) {

        alreadyin = true;

      };

    });

    if (!alreadyin) {

      html += '<span class="add"><a><span class="glyphicon glyphicon-plus" aria-hidden="true"><span class="arrow">&#57490;</span></span></a></span>';

    } else {

      html += '<span class="remove"><a><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a></span>';

    }

  }
  html += '</li>';

  return html;
};

Drupal.theme.prototype.ChatGroupMemberCount = function (count) {

  var html = '';

  //Don't show count for 1to1 group

  if (!chat.groups[chat.user.activegroup].is121) {

    html += '<span class="glyphicon glyphicon-user" aria-hidden="true"></span>';
    html += '<span>' + count + '</span>';

  }

  return html;

};

Drupal.theme.prototype.GroupMemberListItem = function (user, group) {

  var html = '';
  var onlineclasses = 'online-surround';

  if (jQuery.inArray(user.uid, chat.onlineusers) > -1) {
    onlineclasses += ' online';
  }

  html += '<li data-userid="' + user.uid + '">';
  if (user.avatar) {
    html += '<span class="image-container ' + onlineclasses + '"><img src="' + user.avatar + '"></span>';
  } else {
    html += '<span class="image-container ' + onlineclasses + '"><img src="' + Drupal.theme.prototype.GenericUserImage() + '"></span>';
  }
  html += user.username;
  if (!group.isReadOnly) {
    html += '<a class="group-action group-action-remove-user"><span class="glyphicon glyphicon-remove"></span>' + Drupal.t('Remove') + '</a>';
  }
  html += '</li>';

  return html;
}

Drupal.theme.prototype.MediaCallWindow = function () {

  var html = '';

  html += "<h2>" + Drupal.t("Call in progress.") + "</h2>";

};

Drupal.theme.prototype.MediaCallStream = function () {

};

// Templates for rendering messagetypes

Drupal.theme.prototype.MessageTemplate = {};

Drupal.theme.prototype.MessageTemplate.text = function (contents) {

  var html = '';

  html += '<span class="author" title="at ' + contents.formattedtime.date + ' ' + contents.formattedtime.time + '">' + contents.username + '</span>: ';
  html += '<span class="message-content">' + contents.content + '</span>';

  return html;
}

Drupal.theme.prototype.MessageTemplate.file = function (contents) {

  var html = '';

  var fileinfo = JSON.parse(contents.content);

  fileinfo.size = (fileinfo.size / 1000000).toFixed(2);

  html += '<span class="author" title="at ' + contents.formattedtime.date + ' ' + contents.formattedtime.time + '">' + contents.username + '</span>: ';
  html += '<span class="message-content">' + "<a  class='filelink' href='javascript:void(0)' data-id='" + fileinfo.id + "' data-size='" + fileinfo.size + "' data-peer='" + fileinfo.peerid + "' data-groupid='" + fileinfo.groupid + "'>" + fileinfo.name + " " + "(" + fileinfo.size + "MB)" + '</span>';

  return html;
}

Drupal.theme.prototype.MessageTemplate.groupupdate = function (contents) {

  var html = '';

  contents.content = JSON.parse(contents.content);

  if (contents.content.action === 'add') {

      html += '<span class="message-content">' + Drupal.t("!user has joined the group.", {
        '!user': '<b>' + contents.username + '</b>'
      }) + '</span>';
  }

  return html;

};

Drupal.theme.prototype.MessageTemplate.downloadedfile = function (contents) {

  var html = '';

  var fileinfo = contents.content;

  html += '<span class="author" title="at ' + contents.formattedtime.date + ' ' + contents.formattedtime.time + '">' + contents.username + '</span>: ';
  html += '<span class="message-content">' + '<a href="' + fileinfo.link + '" download="' + fileinfo.filename + '">' + Drupal.t("Download @filename", {
    '@filename': fileinfo.filename
  }) + '</a></span>';

  return html;
};

Drupal.theme.prototype.MessageTemplate.mediacall = function (contents) {
  var html = '';

  if (contents.content.action === 'end') {
    html += '<span class="message-content">' + Drupal.t("<b>Call ended</b> at @time (@duration)", {
      '@time': contents.formattedtime.date + ' ' + contents.formattedtime.time,
      '@duration': chat.formatTimeInterval(contents.content.timestamp)
    }) + '</span>';
  } else {
    html += 'Media call.';
  }

  return html;

};

Drupal.theme.prototype.TimeRemaining = function(time) {
  var difference = time - Date.now();

  // Less than 1 minute
  if (difference < 60000) {
    return "<1 min";
  }

  // Less than 1 hour
  else if (difference < 3.6e6) {
    return Math.round(difference / 60000).toString() + ' min';
  }

  // Less than 24 hours
  else if (difference < 86400000) {
    return Math.round(difference / 3.6e6).toString() + ' h';
  }
  // Less than 48 hours
  else if (difference < 86400000*2) {
    return "tomorrow";
  }

  else {

    var reldate = new Date(time);

    return reldate.getDate() + '/' + (reldate.getMonth() + 1) + '/' + reldate.getFullYear();

  }
};

Drupal.theme.prototype.MeetingTime = function (starttime, endtime) {

  var starttime = new Date(starttime);
  var endtime = new Date(endtime);

  if (starttime > Date.now()) {

    return Drupal.theme.prototype.TimeRemaining(starttime);

  } else if (endtime > Date.now()) {
    return "in progress";
  } else {
    return "finished";
  }
};

Drupal.theme.prototype.MeetingAgendaChecklist = function (agenda, entityref) {

  var html = '';

  html += '<div class="agenda-items" data-entityref="' + entityref + '">';
  html += '<h3>Agenda</h3>';

  try {
    agenda = JSON.parse(agenda);
  } catch (e) {
    // try with given agenda
  }

  agenda.forEach(function (element, index) {

    if (element['field_agenda_item']['und'] && element['item_id']) {

      var checkboxname = element['field_agenda_item']['und'][0]['safe_value'];
      var itemid = element['item_id'];

      var checked = '';

      if (element['field_agenda_item_completed']['und'][0]['value'] == 1) {
        checked = 'checked ';
      };

      html += '<label><input type="checkbox" name="agenda-item" value="' + checkboxname + '" data-itemid="' + itemid + '" ' + checked + '/> ' + checkboxname + '</label>';

    }

  });

  html += '</div>';

  return html;

};

/* Timeline */

Drupal.theme.prototype.TimelinePost = function (post) {
  var html = '';

  // message. : _id, content, groupid, userid, username
  // group. : _id, avatar, name, etc.


  var itemClasses;
  var groupLink = document.location.protocol + '//' + document.location.hostname + document.location.pathname + 'node/' + post.group.entityref;


  html += '<div class="timeline-item ' + itemClasses + '">';
  html += '  <div class="timeline-item-header">';
  html +=       '<a href="' + groupLink + '">' + post.group.name + '</a>';
  html += '  </div>';
  html += '  <div class="timeline-item-body">';
  html +=       Drupal.theme.prototype.ChatMessage(post.message);
  html += '  </div>';
  html += '  <div class="timeline-item-body">';
  html += '    ';
  html += '  </div>';
  html += '</div>';

  return html;
};
