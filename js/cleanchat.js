/*jslint browser: true nomen: true plusplus: true */
/*global $, jQuery, socket, console, alert, prompt, confirm, Drupal*/

// Container for the chat app's global variables

var chat = {};

(function ($) {
  'use strict';

      //Disable functionality if not supported by browser (powered by Modernizer). Requirements is an array of Modernizer class names.

  window.checksupport = function (requirements) {

//    $.each(requirements, function (number, requirement) {
//
//      if (!$("html").hasClass(requirement)) {
//
//        console.warn(requirement + " is not supported by this browser");
//        return false;
//
//      }
//
//    })

    //Supported

    return true;

  };

  //Global settings

  chat.settings = {

    server: $.cookie("Drupal.visitor.chatserver"),
    peerserver: $.cookie("Drupal.visitor.peerserver"),
    peerport: $.cookie("Drupal.visitor.peerport"),
    systemuser: '1'

  };

  //Data for the current userid

  chat.user = {

    token: $.cookie("Drupal.visitor.chattoken"),
    id: $.cookie("Drupal.visitor.chatid"),
    peer: null,
    activegroup: null,
    activemediacall: null,
    activevideocall: null,
    totalunread: 0,
    debug: false,
    files: {},
    streams: [],
    calls: [],
    timers: [],
    notifications: {},
    isLatestSocket: false

  }

  chat.page = {

    window: false,
    userpage: null,
    userpagegroup: null,
    popout: false

  }

  chat.onlineusers = [];

  //Change popout setting to true on popout

  if (document.location.pathname === "/chat") {

    chat.page.window = true;
    chat.page.popout = true;

    //Run keepalive connection between both windows

    window.setInterval(function () {

      if (window.opener) {
        window.opener.chat.popup = window;
      }

    }, 500)

  };

  //Container for the user's chat groups

  chat.groups = {};

  //Container for the cached user objects

  chat.users = {};

  // Placeholders for events

  chat.events = {

    ready: function () {

    },

    userpageready: function () {

    },

    newmessage: function (message) {


    },

    remove: function (data) {

    },

    edit: function (data) {

    },

    messagesfetched: function (groupid, messages) {


    },

    groupupdated: function () {


    },

    userlookup: function (results) {

    },

    unreadupdated: function () {

    },

    activegroupchanged: function () {

    },

    mediacallstarted: function () {

    },

    mediacallincoming: function () {

    },

    mediacallbegins: function () {

    },

    mediacalladdstream: function () {

    },

    mediacallremovestream: function () {

    },

    mediacallend: function () {

    },

    onlineusersupdated: function () {


    }

  };

  //Run authentication call on page load

  socket.emit("pair", {
    token: chat.user.token,
    userid: chat.user.id
  });

  socket.on("latest_socket", function(data) {
    chat.user.latestSocket = data;

    if (chat.user.latestSocket === socket.io.engine.id) {
      chat.user.isLatestSocket = true;
    } else {
      chat.user.isLatestSocket = false;
    }
  });

  socket.on("connect", function () {

    socket.emit("pair", {
      token: chat.user.token,
      userid: chat.user.id
    });

  });

  //Reauthentication if authentication fails

  chat.reauthenticate = function () {

    jQuery.get("/reauthenticate", function (data) {
      data = JSON.parse(data);

      if (data.success) {

        $.cookie('Drupal.visitor.chattoken', null, { path: '/' });
        $.cookie('Drupal.visitor.chatid', null, { path: '/' });
        $.cookie('Drupal.visitor.chatserver', null, { path: '/' });
        $.cookie('Drupal.visitor.peerserver', null, { path: '/' });
        $.cookie('Drupal.visitor.peerport', null, { path: '/' });

        
        $.cookie('Drupal.visitor.chattoken', data.chattoken, { path: '/' });
        $.cookie('Drupal.visitor.chatid', data.chatid, { path: '/' });
        $.cookie('Drupal.visitor.chatserver', data.chatserver, { path: '/' });
        $.cookie('Drupal.visitor.peerserver', data.peerserver, { path: '/' });
        $.cookie('Drupal.visitor.peerport', data.peerport, { path: '/' });

        chat.user.id = data.chatid;

        chat.user.token = data.chattoken;

        socket.emit("pair", {
          token: chat.user.token,
          userid: chat.user.id
        });

      } else {

        console.error("Authentication failed");

      }
    }, "json");

  };

  socket.on("pair", function (data) {

    if (data) {

      chat.init();

    } else {

      chat.reauthenticate();

    }

  });

  chat.fetchgroups = function (callback) {

    //Clear groups object before fetching

    chat.groups = {};

    $.get(chat.settings.server + '/fetch/groups', {
      userid: chat.user.id,
      token: chat.user.token
    }, function (grouplist) {

      grouplist.forEach(function (group) {

        if (group.agenda) {
          group.agenda = JSON.parse(group.agenda);
        }

        chat.groups[group._id] = group;

      });

      if (callback) {

        callback();

      }

    }, "JSON");

  };

  chat.peerconnect = function (callback) {

    $.post(chat.settings.server + '/peer', {
      userid: chat.user.id
    }, function (peer_data) {

      var customConfig;

      // Call XirSys ICE servers
      $.ajax({
        type: "POST",
        url: "https://api.xirsys.com/getIceServers?",
        data: {
          ident: "adamclarey",
          secret: "0a3ad990-a758-467b-ae0f-c4a76c0ed9bb",
          domain: "hub.citywebconsultants.co.uk",
          application: "default",
          room: "default",
          secure: 1
        },
        success: function (data, status) {
          //data = JSON.parse(data);
          customConfig = data.d;

          chat.user.peer = new Peer(peer_data, {
            config: customConfig,
            host: chat.settings.peerserver,
            port: chat.settings.peerport
          });

          if (chat.peerhandlers) {
            chat.peerhandlers(chat.user.peer);
          };

          if (callback) {
            callback();
          };
        }
      });
    });

  };

  chat.fetchunreadcounts = function (callback) {

    $.get(chat.settings.server + '/group/unread', {
      userid: chat.user.id,
      token: chat.user.token
    }, function (unread) {

      if (unread !== 0) {

        //Unread object is a list of group objects with an unread count

        var group;

        for (group in unread) {

          if (chat.groups[group]) {

            chat.groups[group].unread = unread[group];

          }

        };

      }

      if (callback) {

        callback();

      }

      if (chat.events.unreadupdated) {

        chat.events.unreadupdated();

      }

    }, 'json');


  };


  //Initialise chat function. Called whenever a socket connection is made or reloaded.

  chat.init = function () {

    chat.fetchgroups(function () {

      chat.peerconnect(function () {

        chat.fetchunreadcounts(function () {

          // If user page
          if ($('body').hasClass("page-user-") && !$('body').hasClass("page-user-" + chat.user.id)) {

            chat.loaduserprofile();

          }

          chat.events.ready();

        });

      });

    });

  };

  chat.debuglog = function (level, message) {
    if (chat.user.debug) {
      console.log(message);
    }
  };

  chat.loaduserprofile = function () {
    // Work out the userid of the user page we're looking at
    $('body').attr('class').split(/\s+/).forEach(function (element, index) {

      if (element.indexOf("page-user-") === 0 && element.length > "page-user-".length) {
        chat.page.userpage = element.replace("page-user-", "").toString();
      }

    });

    // Get one-to-one chat with this user if it exists.
    var ready = false;
    if (chat.page.userpage) {

      $.each(chat.groups, function (index, element) {
        if (element.is121 === true) {
          if ((element.members[0].userid == chat.page.userpage && element.members[1].userid == chat.user.id) || (element.members[1].userid == chat.page.userpage && element.members[0].userid == chat.user.id)) {
            chat.page.userpagegroup = element._id;
            chat.fetchgroupmessages(element._id, function (data) {
              chat.events.userpageready();
            });
            ready = true;
          }
        }
      });

      if (!ready) {
        chat.events.userpageready();
      }
    }
  }

  chat.sendmessage = function (content, type, group) {

    $.post(chat.settings.server + '/message/add', {
      userid: chat.user.id,
      token: chat.user.token,
      messagetype: type,
      content: content,
      groupid: group

    }, 'json');

  };

  chat.focuscheckin = function () {
    socket.emit('focuscheckin', {userid: chat.user.id, token: chat.user.token});
  }

  //Change MongoDB id to timestamp

  chat.mongotime = function (mongoid) {

    return new Date(parseInt(mongoid.substr(0, 8), 16) * 1000);

  }

  //Message received event

  socket.on("message", function (message) {

    message.timestamp = chat.mongotime(message._id);

    var group = chat.groups[message.groupid];

    group.lastupdated = message.timestamp;

    // Check if group exists
    if (group) {

      if (message.groupid === chat.page.userpagegroup) {

        // Instantly mark read

        socket.emit("groupcheckin", {
          groupid: chat.page.userpagegroup,
          userid: chat.user.id,
          token: chat.user.token
        });

        chat.events.unreadupdated();

      } else {

        //Update unread count for group

        if ((message.groupid !== chat.user.activegroup) && (message.userid !== chat.user.id)) {

          if (!group.unread) {

            group.unread = 0;

          }

          group.unread += 1;

          if (chat.events.unreadupdated) {

            chat.events.unreadupdated();

          };

        }

      };

      //Create group messages object if undefined;

      if (!group.messages) {

        group.messages = {};

      }

      group.messages[message._id] = message;

      chat.events.newmessage(message);

      // Re-set active group in order to mark new messages as read.
      if (chat.user.activegroup) {
        chat.setactivegroup(chat.user.activegroup);
      }

    }

  });

  chat.editmessage = function (messageid, type, content) {

    jQuery.post(chat.settings.server + '/message/edit', {
      userid: chat.user.id,
      token: chat.user.token,
      messageid: messageid,
      content: content,
      messagetype: type
    }, 'json');

  };

  chat.deletemessage = function (messageid) {

    jQuery.post(chat.settings.server + '/message/remove', {
      userid: chat.user.id,
      token: chat.user.token,
      messageid: messageid
    }, 'json');

  };

  chat.fetchgroupmessages = function (groupid, callback) {

    var fetched = function (messages) {

      if (chat.groups[groupid]) {

        if (!chat.groups[groupid].messages) {
          chat.groups[groupid].messages = {};
        }

        $.each(messages, function (index, element) {

          chat.groups[groupid].messages[element._id] = element;

        })

      }

      if (chat.events.messagesfetched) {

        chat.events.messagesfetched(groupid, messages);

      };

      if (callback) {

        callback();

      }

    };

    jQuery.get(chat.settings.server + '/fetch/group/messages', {
      userid: chat.user.id,
      token: chat.user.token,
      groupid: groupid
    }, fetched, 'json');

  };

  chat.creategroup = function (members, is121, name, callback) {
    var groupdata;

    if (!name) {

      name = "default";

    }

    if (members) {
      groupdata = 'name=' + name;
      groupdata += '&userid=' + chat.user.id;
      groupdata += '&token=' + chat.user.token;
      if (is121) {
        groupdata += '&is121=true';
      }
      members.forEach(function (element) {
        groupdata += '&members=' + element;
      });

      jQuery.ajax({
        type: 'POST',
        url: chat.settings.server + '/group/add',
        data: groupdata,
        success: function (data) {

          if (callback) {

            callback(data);

          }
        }

      }, 'json');
    }

  };

  chat.fetchgroup = function (groupid) {

    $.get(chat.settings.server + '/fetch/group', {
      userid: chat.user.id,
      token: chat.user.token,
      groupid: groupid
    }, function (group) {

      group = group[0];

      var property;

      if (!chat.groups[groupid]) {

        chat.groups[groupid] = group;

      } else {

        for (property in group) {

          if (property != "messages" && property != "unread") {

            chat.groups[groupid][property] = group[property];

          };

        }
      }

      // Group updated

      chat.fetchunreadcounts(function () {

        if (chat.events.groupupdated) {

          chat.events.groupupdated(groupid);

        }

      });

    }, "JSON");

  };


  chat.addtogroup = function (groupid, userid) {

    jQuery.post(chat.settings.server + '/group/update/addmember', {
      userid: chat.user.id,
      token: chat.user.token,
      members: userid,
      groupid: groupid
    }, 'json');

  };

  chat.removefromgroup = function (groupid, userid) {

    jQuery.post(chat.settings.server + '/group/update/removemember', {
      userid: chat.user.id,
      token: chat.user.token,
      members: userid,
      groupid: groupid
    }, 'json');

  };


  chat.renamegroup = function (groupid, name) {

    jQuery.post(chat.settings.server + '/group/update/name', {
      userid: chat.user.id,
      token: chat.user.token,
      name: name,
      groupid: groupid
    }, 'json');

  };

  //Something's changed on the chat server

  socket.on("notification_message", function (message) {

    chat.debuglog(0, message);

    var group = chat.groups[message.groupid];

    if (message.action === "remove") {

      if (group) {

        delete group["messages"][message.messageid];

      }

    }

    if (message.action === "edit") {

      if (group && group["messages"][message.messageid]) {

        group["messages"][message.messageid].content = message.content;

      };


    }

    if (message.action === "addgroup") {

      chat.fetchgroup(message.groupid);

    }

    if (message.action === "addmember") {

      chat.fetchgroup(message.groupid);

    }


    if (message.action === "removemember") {

      chat.fetchgroup(message.groupid);

    }

    if (message.action === "name") {

      chat.fetchgroup(message.groupid);

    }

    if (message.action === "removedfromgroup") {

      delete chat.groups[message.groupid];

    }

    if (message.action === 'sync') {

      chat.fetchgroup(message.groupid);

    }

    //Call a UI function

    if (chat.events[message.action]) {

      chat.events[message.action](message);

    }

  });

  chat.userslookup = function (search, callback) {

    var output = function (result) {

      callback(result);

    };

    jQuery.get(chat.settings.server + '/usersearch', {
      userid: chat.user.id,
      token: chat.user.token,
      name: search
    }, output, 'json');

  };

  chat.updateagendaitem = function (action, itemid, entityref) {
    if (action == 'check' || action == 'uncheck') {

      // Post to api
      jQuery.get('/api/updateagendaitem', {
        itemid: itemid,
        entityref: entityref,
        action: action
      }, 'json');

    }

  }

  //Change active group

  chat.setactivegroup = function (groupid) {

    if (!chat.groups[groupid]) {

      return false;

    };

    var callback = function () {

      chat.user.activegroup = groupid;

      socket.emit("groupcheckin", {
        groupid: groupid,
        userid: chat.user.id,
        token: chat.user.token
      });

      chat.groups[groupid].unread = 0;

      chat.events.unreadupdated();

      if (chat.events.activegroupchanged) {

        chat.events.activegroupchanged();

      };

    };

    if (!chat.groups[groupid].messages) {

      chat.fetchgroupmessages(groupid, callback);

    } else {

      callback();

    }

    //Reset typing object

    chat.typing = {};

  };

  chat.start1to1 = function (userid, callback) {

    var grouptoload;
    var group;

    for (group in chat.groups) {

      if (chat.groups[group].is121) {

        $.each(chat.groups[group].members, function (index, element) {

          if (element.userid === userid) {

            grouptoload = group;

          }

        });

      }

    };

    if (!grouptoload) {

      chat.creategroup([chat.user.id, userid], true, null, function (data) {

        if (callback) {

          callback(data);

        }

      });

    } else {

      if (callback) {

        callback(grouptoload);

      }

    }

  };

  // Broadcast heartbeat to track activity
  window.setInterval(function () {
    socket.emit("alive", {
      userid: chat.user.id,
      token: chat.user.token
    });
  }, 2000);

  socket.on("users_online", function (data) {
    chat.onlineusers = data.users;
    chat.events.onlineusersupdated();
  });

  //Fetch peer

  chat.fetchpeer = function (list, callback) {

    list = list.join("+");

    jQuery.post(chat.settings.server + '/peerlist', {
      userid: chat.user.id,
      userlist: list
    }, callback, 'json');

  };

  //Send peer message

  chat.peersend = function (client, data) {

    //Check if client is a peerid or a userid

    if (client.peerid) {

      var con = chat.user.peer.connect(client.peerid);

      con.on('open', function () {

        con.send(data);

      });

    }

    if (client.userid) {

      var list = [];

      client.userid.forEach(function (element) {

        list.push(element);

      });

      chat.fetchpeer(list, function (peers) {

        peers.forEach(function (element) {

          var con = chat.user.peer.connect(element);

          con.on('open', function () {

            con.send(data);

          });

        });

      });

    }

  };

  //Respond to peer messages

  chat.peerhandlers = function (peer) {

    peer.on('connection', function (conn) {
      conn.on('data', function (data) {

        if (data.type === "filerequest") {

          var author = data.data.author;
          var fileid = data.data.file;

          if (chat.user.files[fileid]) {

            chat.peersend({
              peerid: [conn.peer]
            }, {
              type: 'file',
              data: chat.user.files[fileid]
            });
          }

        } else if (data.type === "file") {

          data = data.data;

          var blob = new Blob([data.file]);

          var savecontent = function (fileContents, fileName) {

            var group = chat.groups[data.details.groupid];

            $.each(group.messages, function (index, element) {

              if (element.content.file) {


                var filedata = JSON.parse(element.content.file);

                if (filedata.id === data.details.id) {

                  var targetmessage = group.messages[index];

                  targetmessage.content.downloadedfile = {
                    filename: fileName,
                    link: URL.createObjectURL(blob)
                  };

                  delete targetmessage.content.file;

                  if (chat.events.downloaded) {

                    chat.events.downloaded(data.details.id, targetmessage);

                  }
                }

              }

            });

          }

          savecontent(blob, data.details.name);

        };

      });

    });

    // Peer mediacalls
    chat.user.peer.on('call', function (call) {

      chat.capture(function (stream) {

        chat.user.streams.push(stream);
        chat.user.calls.push(call);

        call.answer(stream);

      }, true, chat.user.activevideocall);

      call.on("stream", function (stream) {
        var user = parseInt(call.peer.split("u")[0], 10);

        chat.events.mediacalladdstream(user, stream);

      });

      call.on('close', function () {

        if ($('#chat-panel').hasClass('expanded')) {
          chat.toggleExpanded(true);
        }
        var usercalling = parseInt(call.peer.split("u")[0], 10);
        chat.events.mediacallremovestream(usercalling);

      });

    });
  }

  chat.fetchgroupusers = function (groupid, callback) {
    $.get(chat.settings.server + '/fetch/group/users', {
      userid: chat.user.id,
      token: chat.user.token,
      groupid: groupid
    }, function (userlist) {

      if (callback) {
        callback(userlist);
      }

      return userlist;
    }, 'json');
  };

  // Mediacall

  chat.mediacallmake = function () {

    socket.emit('mediacallstart', {
      userid: chat.user.id,
      token: chat.user.token,
      groupid: chat.user.activegroup
    });

    chat.events.mediacallstarted();

  };

  chat.mediacallaccept = function (voice, video) {

    socket.emit('mediacallaccept', {
      userid: chat.user.id,
      token: chat.user.token,
      video: video,
      voice: voice,
      mediacallid: chat.user.activemediacall
    });

  };

  socket.on('mediacallinit', function (data) {

    // Pair with the others
    chat.acceptcall(data.call.members, data.voice, data.video);
  });

  chat.mediacallhangup = function () {

    socket.emit('mediacallhangup', {
      userid: chat.user.id,
      token: chat.user.token,
      mediacallid: chat.user.activemediacall
    });

    chat.removeallstreams();

    $("#mediacall-window video").remove();

    chat.events.mediacallend();

  };

  chat.removeallstreams = function () {

    $.each(chat.user.calls, function (index, element) {

      chat.user.calls[index].close();

    });

    $.each(chat.user.streams, function (index, element) {

      chat.user.streams[index].stop();

    });

  };

  chat.acceptcall = function (calling, voice, video) {

    chat.capture(function (mystream) {

      chat.fetchpeer(calling, function (list) {

        list.forEach(function (element) {

          var call = chat.user.peer.call(element, mystream);

          chat.user.streams.push(mystream);
          chat.user.calls.push(call);

          call.on('stream', function (stream) {

            var usercalling = parseInt(call.peer.split("u")[0], 10);

            chat.events.mediacalladdstream(usercalling, stream);

          });

          call.on('close', function () {

            var usercalling = parseInt(call.peer.split("u")[0], 10);

            chat.events.mediacallremovestream(usercalling);

          });

        });

      });

    }, voice, video);

  };

  // Get audio/video stream
  chat.capture = function (callback, audio, video) {

    var options = {
      video: video,
      audio: audio
    }

    var fail = function (error) {

      return error;

    };

    var success = function (stream) {

      chat.events.mediacalladdstream(chat.user.id, stream);
      callback(stream);

    };

    //Browser compatibility for mediastream

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    navigator.getUserMedia(options, success, fail);

  };

  socket.on('mediacallhungup', function (data) {

    data.members.forEach(function (element) {

      chat.events.mediacallremovestream(element);

    });

    if (data.members.length < 2) {

      chat.events.mediacallend();
      chat.removeallstreams();

    };

  });

  socket.on('mediacallstart', function (data) {
    chat.user.activemediacall = data.id;
    chat.events.mediacallbegins();

    var doescontainme = false;
    data.members.forEach(function (element) {
      if (element === chat.user.id) {
        doescontainme = true;
      }
    });

    if (!doescontainme) {
      chat.events.mediacallincoming();
    }
  });

  //Local storage event listener

  jQuery(window).bind('storage', function (e) {

    if (chat.user.timers.unread) {

      window.clearTimeout(chat.user.timers.unread);

    };

    chat.user.timers.unread = window.setTimeout(function () {

      var data = e.originalEvent;

      if (data["key"] === "unread") {

        var unread = JSON.parse(data["newValue"]);


        $.each(chat.groups, function (index, element) {

          if (unread[index]) {

            chat.groups[index].unread = unread[index];

          } else {

            chat.groups[index].unread = 0;

          }

        });

        if (chat.events.unreadupdated) {
          chat.events.unreadupdated();
        }

      };

    }, 500);

  });

  //Get typing meter

  chat.typing = {};

  chat.showtyping = function () {

    var message = "";

    var typer,
      count = 0;

    for (typer in chat.typing) {

      if (chat.typing[typer].uid) {

        if (chat.typing[typer].uid !== chat.user.id) {

          if (count === 0) {

            message += chat.typing[typer].username;

          } else {

            message += "& " + chat.typing[typer].username;

          }

          count += 1;

        }
      }

    };

    if (count === 1) {

      message += " is typing";

    } else if (count > 1) {

      message += " are typing";

    }

    if (chat.events.typing) {

      chat.events.typing(message);

    }

  };

  socket.on("typingstart", function (data) {

    if (data.group === chat.user.activegroup || data.group === chat.user.lastactivegroup) {

      chat.typing[data.user.id] = data.user;
      chat.showtyping();

    }

  });

  socket.on("typingstop", function (data) {

    if (data.group === chat.user.activegroup || data.group === chat.user.lastactivegroup) {

      delete chat.typing[data.user.id];
      chat.showtyping();

    }

  });

  $(".message-textfield").keyup(function () {

    if ($(".message-textfield").val().length > 0) {

      if (!chat.typing[chat.user.id]) {

        socket.emit("typingstart", {
          userid: chat.user.id,
          groupid: chat.user.activegroup
        });

      }

      chat.typing[chat.user.id] = {
        uid: chat.user.id
      };

    } else {

      socket.emit("typingstop", {
        userid: chat.user.id,
        groupid: chat.user.activegroup
      });

      delete chat.typing[chat.user.id];

    }
  });

}(jQuery));
