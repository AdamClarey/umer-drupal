(function ($) {

  //Function for checking if the scrollbar is at bottom of chat window

  chat.scrollbottom = function () {

    var scrolltop = $("#conversation").scrollTop();

    var outerheight = $("#conversation").outerHeight();

    var scrollheight = $("#conversation")[0].scrollHeight;

    return ((scrollheight - scrolltop) === outerheight);

  };

  chat.events.newmessage = function (message) {

    chat.timelinepush(message);

    if (message.groupid === chat.user.activegroup || message.groupid === chat.user.lastactivegroup) {

      var atbottom = chat.scrollbottom();
      $("#conversation").append(Drupal.theme.prototype.ChatMessage(message));

      if (atbottom) {

        $("#conversation")[0].scrollTop = $("#conversation")[0].scrollHeight

      };

    }

    if (chat.page.userpage && message.groupid === chat.page.userpagegroup) {

      $("#user-chat-window-top").append(Drupal.theme.prototype.ChatMessage(message));
      $("#user-chat-window-top")[0].scrollTop = $("#user-chat-window-top")[0].scrollHeight;

    } else if ((message.userid !== chat.user.id) && !chat.user.activegroup && chat.user.isLatestSocket) {
      var markup = Drupal.theme.prototype.PopupMessage(message);

      if ("Notification" in window) {

        var ping = function () {

          if (message.content.text) {

            message.content.text = jQuery('<div/>').html(message.content.text).text();

          };

          if (message.username) {

            chat.user.notifications[message._id] = new Notification("New message from " + message.username, {
              icon: chat.groups[message.groupid].avatar,
              body: message.content.text
            });

            // Make notifications auto dismiss
            window.setTimeout(function () {
              chat.user.notifications[message._id].close();
            }, 10000);

            chat.user.notifications[message._id].onclick = function () {

              chat.setactivegroup(message.groupid);
              if (!chat.page.popout) {

                if (!chat.page.window) {

                  Drupal.toggleSlidein();

                }

                window.focus();

                $('html, body').scrollTop($("#block-hub-chat-chat-window").offset().top - 80);
              } else {

                chat.popup.focus();

              };

              chat.user.notifications[message._id].close();

              //Delete

              delete chat.user.notifications[message._id];

            };

            chat.user.notifications[message._id].onclose = function () {

              delete chat.user.notifications[message._id];

            };

          };

        }


        if (message.content.text && !chat.page.window) {


          // Check if the browser supports notifications

          if ("Notification" in window && Notification.permission === "granted") {

            ping();

          }

        }

        // Otherwise, we need to ask the user for permission
        else if ("Notification" in window && Notification.permission !== 'denied') {
          Notification.requestPermission(function (permission) {
            // If the user is okay, let's create a notification
            if (permission === "granted") {
              ping();
            }
          });
        } else {

          if ($(".notification-area").length === 0) {

            $("body").append("<div class='notification-area'></div>");

          };

          var popup = $(document.createElement("div")).html(markup);

          $(".notification-area").append(popup);

          $(popup).find(".chat-notification").fadeIn('fast');

          window.setTimeout(function () {

            $(popup).fadeOut();

          }, 10000);

          $(popup).on("click", ".close-popup", function () {

            $(popup).fadeOut("fast", function () {

              $(popup).remove();

            })

          });

        }

      }

    }

  };

  chat.events.unreadupdated = function () {

    var count = 0;

    var group;

    var unreadobject = {}; //Object for storing unread counts so they can be passed between windows in local storage

    for (group in chat.groups) {

      if (chat.groups[group].unread) {

        unreadobject[group] = chat.groups[group].unread;

        count += chat.groups[group].unread;

      }

    };

    if (count > 0) {

      jQuery("a.chat-envelope").addClass("unread");


    } else {

      jQuery("a.chat-envelope").removeClass("unread");


    }


    var previouscount = chat.user.totalunread;

    chat.user.totalunread = count;

    if (previouscount === 0 && count > 0) {

      document.title = "(" + count + ")" + " " + document.title;

    } else if (previouscount > 0 && count === 0) {

      document.title = document.title.replace("(" + previouscount + ")" + " ", "");

    } else {

      document.title = document.title.replace("(" + previouscount + ")" + " ", "(" + count + ")" + " ");

    }

    chat.updategrouplist();

    if (checksupport(["localstorage"])) {

      localStorage.setItem("unread", JSON.stringify(unreadobject));

    }

  };

  chat.updategrouplist = function () {

    $("#grouplist").html("");

    var group;

    var output = [];

    for (group in chat.groups) {

      output.push(chat.groups[group]);

    };

    //Sort

    var sorted = output.sort(function (a, b) {
      if (a.lastupdated > b.lastupdated) {
        return -1;
      }
      if (a.lastupdated < b.lastupdated) {
        return 1;
      }
      // a must be equal to b
      return 0;

    });

    $.each(sorted, function (index, element) {

      $("#grouplist").append(Drupal.theme.prototype.GroupListItem(element));

    });

    $("#chat li.group[data-groupid='" + chat.user.activegroup + "']").addClass("active");

  };

  //Select a group in the groups list

  $("#chat").on("click", "li.group", function (e) {

    var groupid = $(this).attr("data-groupid");
    chat.setactivegroup(groupid);
    if ($('#chat').hasClass('mini')) {
      $('.chat-head').click();
    }

  });

  chat.events.userpageready = function () {
    if (chat.page.userpage) {
      $("#user-chat-window").show();

      if (chat.page.userpagegroup) {
        $.each(chat.groups[chat.page.userpagegroup].messages, function (index, element) {
          $("#user-chat-window-top").append(Drupal.theme.prototype.ChatMessage(element));
          $("#user-chat-window-top")[0].scrollTop = $("#user-chat-window-top")[0].scrollHeight;
        });
      } else {
        $("#user-chat-window-top").html('<div class="chat-only-message">Just send a message to begin a chat with this user.</div>');
      }
    }
  };

  chat.formatTime = function (timestamp) {

    var day = timestamp.getDate();
    var month = timestamp.getMonth() + 1;
    var year = timestamp.getFullYear();

    var addzeroes = function (num) {
      if (num.toString().length == 1) {
        num = "0" + num.toString();
      }
      return num;
    };

    var hours = addzeroes(timestamp.getHours());
    var mins = addzeroes(timestamp.getMinutes());
    var seconds = addzeroes(timestamp.getSeconds());

    return {
      date: day + "/" + month + "/" + year,
      time: hours + ":" + mins + ":" + seconds
    };
  };

  chat.formatTimeInterval = function (milliseconds) {

    var numberEnding = function (number) {
      return (number > 1) ? 's' : '';
    }

    var temp = Math.floor(milliseconds / 1000);
    var years = Math.floor(temp / 31536000);
    if (years) {
      return years + ' year' + numberEnding(years);
    }
    var days = Math.floor((temp %= 31536000) / 86400);
    if (days) {
      return days + ' day' + numberEnding(days);
    }
    var hours = Math.floor((temp %= 86400) / 3600);
    if (hours) {
      return hours + ' hour' + numberEnding(hours);
    }
    var minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) {
      return minutes + ' minute' + numberEnding(minutes);
    }
    var seconds = temp % 60;
    if (seconds) {
      return seconds + ' second' + numberEnding(seconds);
    }
    return 'less than a second';

  };

  //Populate chat conversation with messages

  chat.events.activegroupchanged = function () {

    //Theme member count

    var membercount = chat.groups[chat.user.activegroup].members.length;

    // Ensure members list is hidden and only displayed if it actually contains members
    $('#group-members').html('');
    $('#group-members').hide();
    $(".member-count-surround").show();

    $("#member-count").html(Drupal.theme.prototype.ChatGroupMemberCount(membercount))

    if ($("#member-count").html() === '') {
      $(".member-count-surround").hide();
    }

    $("#chat li.group").removeClass("active");

    $("#chat li.group[data-groupid='" + chat.user.activegroup + "']").addClass("active");

    //Loop over and style the message

    var messages = chat.groups[chat.user.activegroup].messages;

    var message;

    $("#conversation").html("");

    for (message in messages) {

      $("#conversation").append(Drupal.theme.prototype.ChatMessage(messages[message]));

    };

    $("#group-header h2").html(chat.groups[chat.user.activegroup].name);

    //Scroll to bottom
    document.getElementById("conversation").scrollTop = conversation.scrollHeight;

    // Meeting agenda

    $("#meeting-window").html('');

    if (chat.groups[chat.user.activegroup].agenda) {

      $(".meeting-window-surround").show();

      $("#meeting-window").html(Drupal.theme.prototype.MeetingAgendaChecklist(chat.groups[chat.user.activegroup].agenda, chat.groups[chat.user.activegroup].entityref));

    } else {

      $(".meeting-window-surround").hide();

    }

    // Clear UI buttons

    $(".group-actions .group-action").hide();
    $(".group-actions .group-action").hide();

    // Show correct UI buttons

    $(".group-actions .group-action.group-action-send-file").show();

    if (!chat.user.activemediacall) {
      $(".group-actions .group-action.group-action-videocall").show();
      $(".group-actions .group-action.group-action-voicecall").show();
    }

    if (chat.groups[chat.user.activegroup] && chat.groups[chat.user.activegroup].is121) {
      $(".group-actions .group-action.group-action-add-user").show();
    } else if (chat.groups[chat.user.activegroup] && chat.groups[chat.user.activegroup].isReadOnly) {
      // this is a read-only OG group.
    } else {
      $(".group-actions .group-action.group-action-rename").show();
      $(".group-actions .group-action.group-action-add-user").show();
      $(".group-actions .group-action.group-action-leave").show();
    }

    //Clear typing note

    $("#typing").text("");

  }

  $(".group-action-videocall").click(function () {
    if ($(window).width() > 960 && $('html.cssvhunit').length > 0) {
      chat.toggleExpanded();
    }
    $('.mediacall-window-surround').addClass('is-calling');
    chat.user.activevideocall = true;
    chat.mediacallmake();
  });

  $(".group-action-voicecall").click(function () {
    $('.mediacall-window-surround').addClass('is-calling');
    chat.user.activevideocall = false;
    chat.mediacallmake();
  });

  $(".group-action-hangup").click(function () {
    $('.mediacall-window-surround').removeClass('is-calling');
    chat.mediacallhangup();
    if ($('#chat-panel').hasClass('expanded')) {
      chat.toggleExpanded(true);
    }
  });

  //Send message

  $(".user-submit-message-from").submit(function (e) {

    e.preventDefault();

    var message = $(".user-submit-message-from .message-textfield").val();

    var send = function () {

      chat.sendmessage(message, "text", chat.page.userpagegroup);

      $(".user-submit-message-from .message-textfield").val("");

      var conversation = document.getElementById("conversation");

      conversation.scrollTop = conversation.scrollHeight;

      return false;

    }

    if (chat.page.userpagegroup) {

      send();

    } else {

      // Create one to one group
      chat.creategroup([chat.user.id, chat.page.userpage], true, "default", function (group) {
        if (group && group.length === 24 && group.indexOf("ERROR") !== 0) {

          chat.page.userpagegroup = group;

          $("#user-chat-window-top").html("");

          $(".user-submit-message-from .message-textfield").val("");

          chat.waitingmessage = message;

        }

      });

    }

  });

  $(".submit-message-from").submit(function () {

    var message = $(".submit-message-from .message-textfield").val();

    chat.sendmessage(message, "text", chat.user.activegroup);

    $(".submit-message-from .message-textfield").val("");

    var conversation = document.getElementById("conversation");

    conversation.scrollTop = conversation.scrollHeight;

    return false;

  });

  // Show and hide timestamps

  $("#chat").on('click', '.message .author', function () {
    if ($(this).parent().find('.time').css('display') === 'none') {
      $(this).parent().find('.time').show();
    } else {
      $(this).parent().find('.time').css('display', 'none');
    }
  });

  $("#chat").on('click', '.message .time', function () {
    $(this).css('display', 'none');
  });

  //Edit message

  $("body").on("click", ".message-tools [title=Edit]", function () {

    var message = $(this).parent().parent();

    var messageid = $(message).attr("data-messageid");

    var content = $(message).find(".message-content").html();

    content = prompt("Edit message", content);

    if (content) {

      chat.editmessage(messageid, "text", content);

    }

  });

  //Refresh edited message if in active group

  chat.events.edit = function (message) {

    chat.setactivegroup(chat.user.activegroup);

    if (message.groupid === chat.page.userpagegroup) {
      $("#user-chat-window-top .message[data-messageid='" + message.messageid + "']").replaceWith(Drupal.theme.prototype.ChatMessage(chat.groups[chat.page.userpagegroup].messages[message.messageid]));
    }

  };

  //Delete message

  $("body").on("click", ".message-tools [title=Delete]", function () {

    var message = $(this).parent().parent();

    var messageid = $(message).attr("data-messageid");

    var content = $(message).find(".message-content").html();

    var sure = confirm("Delete this message?\n\n" + "'" + content + "'");

    if (sure) {

      chat.deletemessage(messageid);

    }

  });

  //Remove removed message if in active group

  chat.events.remove = function (message) {

    chat.setactivegroup(chat.user.activegroup);

    if (message.groupid === chat.page.userpagegroup) {
      $("#user-chat-window-top .message[data-messageid='" + message.messageid + "']").remove();
    }

  };

  //Search form

  $("#chat-search").submit(function (e) {

    e.preventDefault(e);
    var search = $("#chat-search").find("#chat-search-field").val();

    if (search.length > 0) {

      chat.userslookup(search, function (results) {

        //Clear search

        $("#chat-search-results").html("");

        $.each(results, function (index, element) {

          $("#chat-search-results").append(Drupal.theme.prototype.UserSearchItem(element));

        });

      });

    } else {

      $("#chat-search-results").html("");

    };

  });

  $("#chat-search").keyup(function () {

    $("#chat-search").submit();

  });

  //Search tab toggle

  chat.togglesearch = function () {

    $("#chat-search-results").html("");

    $(".lookup-title.search").addClass("active");

    $(".lookup-title.recent").removeClass("active");

    $("#chat-search-pane").addClass('open');
    $("#groupbar").removeClass('open');

  };


  chat.togglerecent = function () {

    $("#chat-search-pane").removeClass('open');
    $("#groupbar").addClass('open');

    $(".lookup-title.recent").addClass("active");

    $(".lookup-title.search").removeClass("active");

  };

  $(".lookup-title.recent").on("click", chat.togglerecent);
  $(".lookup-title.search").on("click", chat.togglesearch);


  $("#chat-search-results").on("click", "li.user .name", function () {

    var user = $(this).parent().attr("data-userid");

    if (!isNaN(user)) {

      chat.start1to1(user, function (data) {

        if (chat.groups[data]) {
          chat.setactivegroup(data);
        }

        chat.togglerecent();
        if ($('#chat').hasClass('mini')) {
          $('.chat-head').click();
        }

      });

    }

  });

  //Toggle search pane on clicking "add user"

  $(".group-action-add-user").click(function () {

    chat.togglesearch();

  });

  // Add user to group or create group if doesn't already exist

  $("#chat-search-pane").on("click", ".add", function () {

    var userid = $(this).parent().attr("data-userid");

    $(this).hide();

    var currentgroup = chat.groups[chat.user.activegroup];

    if (currentgroup && currentgroup.is121) {

      var members = [];

      $.each(currentgroup.members, function (index, member) {

        members.push(member.userid);

      });

      members.push(userid);

      chat.creategroup(members, false, null, function (data) {

      })

    } else if (chat.user.activegroup) {

      chat.addtogroup(chat.user.activegroup, userid);

    };

  });

  //remove user from group in search pane

  $("#chat-search-pane").on("click", ".remove", function () {

    var userid = $(this).parent().attr("data-userid");

    chat.removefromgroup(chat.user.activegroup, userid);
    chat.togglesearch();

  });

  // Leave group click handler.

  $(".group-action-leave").click(function () {

    var sure = confirm(Drupal.t("Are you sure you want to leave this group?\n\nYou will not be able to participate in this group unless another user adds you to the conversation again."));

    if (sure) {
      chat.removefromgroup(chat.user.activegroup, chat.user.id);
    }

  });

  // Rename group click handler.

  $(".group-action-rename").click(function () {

    var oldname = chat.groups[chat.user.activegroup].name;
    var newname = prompt("New name for group:", chat.groups[chat.user.activegroup].name);

    if (oldname !== newname) {
      chat.renamegroup(chat.user.activegroup, newname);
    }

  });

  chat.events.removedfromgroup = function (data) {

    var group = $("ul li.group[data-groupid='" + data.groupid + "']");

    $(group).remove();

    if (chat.user.activegroup === data.groupid) {

      chat.user.activegroup = null;

    };

  };

  //Open file browse link

  $(".group-action-send-file").click(function () {

    $("#files").click();

  });

  //Trigger filesend operation when file input changes

  $("#files").change(function (event) {

    var file = event.target.files[0];

    var output = {
      name: file.name,
      size: file.size,
      id: chat.user.id + Date.now(),
      peerid: chat.user.peer.id,
      groupid: chat.user.activegroup
    };

    chat.user.files[output.id] = {
      file: file,
      details: output
    };

    //Send file request message

    jQuery.post(chat.settings.server + '/message/add', {
      userid: chat.user.id,
      token: chat.user.token,
      messagetype: "file",
      content: JSON.stringify(output),
      groupid: chat.user.activegroup

    }, 'json');

    //Clear upload widget

    var upload = document.getElementById("fileupload");
    var child = document.getElementById("files");

    $("#files").html('<input type="file" id="files" name="files[]"/>')

  });

  $("#conversation").on("click", ".filelink", function () {

    $($(this)[0]).text("Downloading");

    var filelink = $(this);
    var id = filelink.attr("data-id");
    var peer = filelink.attr("data-peer");

    var message = filelink.parent().parent();

    var author = $(message).attr("data-userid");

    chat.acceptfile(id, author, peer);


  });

  chat.acceptfile = function (id, author, peer) {

    if (true || author !== chat.user.id) {

      //Check if peer exists

      chat.fetchpeer([author], function (list) {

        var exists = false;

        list.forEach(function (element) {

          {
            if (element == peer) {

              exists = true;

            }

          }

        });

        if (!exists) {

          var link = $(".filelink[data-peer='" + peer + "']");

          $(link).html(Drupal.t("File no longer available"));

        } else {

          chat.peersend({
            peerid: peer
          }, {
            type: "filerequest",
            data: {
              file: id,
              author: author
            }
          });

        }

      });

    }

  };

  //Respond to downloaded file

  chat.events.downloaded = function (fileid, message) {

    var messagelink = $(".filelink[data-id='" + fileid + "']").parent().parent();

    messagelink[0].outerHTML = Drupal.theme.prototype.ChatMessage(message);

  };

  // Display group member list

  $('#member-count').click(function () {

    if ($('#group-members').css('display') === 'none') {
      $('#group-members').css('display', 'block');
      chat.fetchgroupusers(chat.user.activegroup, function (users) {
        users.forEach(function (element, index) {
          $('#group-members').append(Drupal.theme.prototype.GroupMemberListItem(element, chat.groups[chat.user.activegroup]));
        });
      });
    } else {
      $('#group-members').html('');
      $('#group-members').css('display', 'none');
    }

  });

  // Remove specific user from chat via list

  $("#group-members").on("click", ".group-action-remove-user", function () {

    chat.removefromgroup(chat.user.activegroup, $(this).parent().attr('data-userid'));
    $(this).parent().remove();

  });

  // Media call events

  // Media call begins: draw UI.
  chat.events.mediacallbegins = function () {

  };

  chat.events.mediacallincoming = function () {
    $(".group-actions .group-action.group-action-videocall").hide();
    $(".group-actions .group-action.group-action-voicecall").hide();
    $(".mediacall-window-surround").show();

    // TODO: This could be a theme function.
    $('body').append('<div id="incoming-wrapper">' +
      '<div id="incoming-box">' +
      '<p>' + Drupal.t('Do you want to accept incoming call?') + '</p>' +
      '<div id="incoming-buttons">' +
      '<div id="incoming-video"><span class="glyphicon glyphicon-facetime-video"></span><span>' + Drupal.t('Video') + '</span></div>' +
      '<div id="incoming-voice"><span class="glyphicon glyphicon-earphone"></span><span>' + Drupal.t('Voice') + '</span></div>' +
      '<div id="incoming-reject"><span class="glyphicon glyphicon-remove"></span><span>' + Drupal.t('Reject') + '</span></div>' +
      '</div>' +
      '</div>' +
      '</div>');
    Drupal.attachBehaviors($('#incoming-wrapper'));

  };

  chat.events.mediacallstarted = function () {

    $(".group-actions .group-action.group-action-videocall").hide();
    $(".group-actions .group-action.group-action-voicecall").hide();
    $(".group-action-hangup").show();
    $(".mediacall-window-surround").show();

  };

  // Media call ends: remove UI.
  chat.events.mediacallend = function () {

    $(".group-action-hangup").hide();
    $(".mediacall-window-surround").hide();
    $(".group-actions .group-action.group-action-videocall").show();
    $(".group-actions .group-action.group-action-voicecall").show();

    chat.user.activemediacall = null;

  };

  // Add a stream to the currently active media call interface.
  chat.events.mediacalladdstream = function (userid, stream) {
    var mediacallwindow = $("#mediacall-window");
    // Remove 'is-calling' class.
    $('.mediacall-window-surround').removeClass('is-calling');
    if ($('video', mediacallwindow).length > 2) {
      $(mediacallwindow).addClass('multi');
    }
    if ($("video[data-id='" + userid + "']").length === 0) {

      var video = document.createElement("video");
      video.src = URL.createObjectURL(stream);
      video.setAttribute("data-id", userid);

      jQuery(video).addClass('chat-video');
      jQuery(mediacallwindow).append(video);
      video.play();

      if (userid === chat.user.id) {
        jQuery(video).addClass("mirrored").prop('muted', true);
      }
    }
  };

  // Remove a stream from the currently active media call interface.
  chat.events.mediacallremovestream = function (userid) {
    $("video[data-id='" + userid + "']").remove();
  };

  chat.events.onlineusersupdated = function () {

    $(".chat-window .online-surround").removeClass("online");

    $.each(chat.onlineusers, function (index, element) {

      var user = $("[data-userid=" + element + "]");
      var closest = user.find(".online-surround");

      closest.addClass("online");

    });
  };

  //Window toggle event

  chat.events.windowtoggle = function () {

    if (chat.page.window) {

      chat.user.activegroup = null;
      chat.page.window = false;

    } else {

      var windowWidth = $('#chat').width();
      if (windowWidth < 540) {
        $('#chat').addClass('mini');
        $('#responsive-heads .chat-head').click();
      } else {
        $('#chat').removeClass('mini');
        chat.togglerecent();
      }

      if (!chat.user.activegroup) {

        var newestgroup = $("#grouplist li.group").first().attr("data-groupid");

        if (newestgroup) {
          chat.setactivegroup(newestgroup);
        }

      }
      chat.page.window = true;
    }

  };

  // Responsiveness
  $(document).ready(function () {
    var windowWidth = $('#chat').width();
    if (windowWidth < 540) {
      $('#chat').addClass('mini');
      $('#responsive-heads .chat-head').click();
    }

    //Fade and disable media call buttons if not available

    if (!checksupport(["canvas", "video", "peerconnection", "datachannel"])) {

      var fail = function () {

        alert("Not supported by your browser. Current browser support is the latest version of Google Chrome and Mozilla Firefox");

      }

      $(".group-action-videocall").css("opacity", "0.3").unbind().click(fail);
      $(".group-action-voicecall").css("opacity", "0.3").unbind().click(fail);
      $(".group-action-send-file").css("opacity", "0.3").unbind().click(fail);

    }

  });

  $(window).resize(function () {
    var windowWidth = $('#chat').width();
    if (windowWidth < 540) {
      $('#chat').addClass('mini');
      $('#responsive-heads .chat-head').click();
    } else {
      if ($('.chat-window .recent').hasClass('active')) {
        chat.togglerecent();
      } else if ($('.chat-window .search').hasClass('active')) {
        chat.togglesearch();
      } else {
        // Just default to recent
        $('#responsive-heads .recent').click();
      }
      $('#chat').removeClass('mini');
    }
  });

  Drupal.behaviors.chatResponsive = {
    attach: function (context, settings) {
      $('#responsive-heads h3', context).click(function () {
        $('#responsive-heads h3.active').removeClass('active');
        $(this).addClass('active');
        $('#chat-panel, .lookup-col-container').removeClass('open');
        $('#' + $(this).data('panel')).addClass('open');
      });
    }
  };

  Drupal.behaviors.chatReceiveButtons = {
    attach: function (context, settings) {
      // Receive incoming call.
      $('#incoming-video', context).click(function () {
        $('#incoming-wrapper').remove();
        if (!chat.page.window) {
          Drupal.toggleSlidein();
        }

        chat.mediacallaccept(true, true);
        $(".group-action-hangup").show();
      });

      $('#incoming-voice', context).click(function () {
        $('#incoming-wrapper').remove();
        if (!chat.page.window) {
          Drupal.toggleSlidein();
        }

        chat.mediacallaccept(true, false);
        $(".group-action-hangup").show();
      });

      $('#incoming-reject', context).click(function () {
        $('#incoming-wrapper').remove();
        $(".group-actions .group-action.group-action-videocall").show();
        $(".group-actions .group-action.group-action-voicecall").show();
      });
    }
  }

  //Open popup

  $("#popout-link").click(function () {

    if (!chat.popup) {
      chat.popup = window.open("chat", "Chat", "height=600,width=760").focus();
    } else {

      chat.popup.focus();

    }

  });

  chat.events.addgroup = function (data) {

    //Wait for group to be loaded before setting as active group

    //Clear timer if it already exists

    if (chat.user.timers[data.groupid]) {

      window.clearInterval(chat.user.timers[data.groupid].timer);
      delete chat.user.timers[data.groupid];

    };

    //Create timer and count (for the timeout if it fails too many times)

    chat.user.timers[data.groupid] = {};
    chat.user.timers[data.groupid].count = 0;
    chat.user.timers[data.groupid].timer = window.setInterval(function () {

      chat.user.timers[data.groupid].count += 1;

      if (chat.user.timers[data.groupid].count > 20) {

        window.clearInterval(chat.user.timers[data.groupid].timer);
        delete chat.user.timers[data.groupid];

      }

      var group = chat.groups[data.groupid];

      if (group && group.members[0].userid === chat.user.id) {

        chat.setactivegroup(data.groupid);
        window.clearInterval(chat.user.timers[data.groupid].timer);
        delete chat.user.timers[data.groupid];

      }

      // User profile chat waiting message
      if (group && group.is121 && ((group.members[0].userid == chat.user.id && group.members[1].userid == chat.page.userpage) || (group.members[1].userid == chat.user.id && group.members[0].userid == chat.page.userpage))) {

        // Check for and send waiting message
        if (chat.waitingmessage) {

          chat.sendmessage(chat.waitingmessage, "text", chat.page.userpagegroup);
          chat.waitingmessage = null;

        }

      }

    }, 100)

  }

  chat.events.groupupdated = function (group) {

    if (group === chat.user.activegroup) {

      chat.setactivegroup(chat.user.activegroup);

    }

  };

  //Lost browser focus

  $(window).blur(function () {

    if (chat.user.activegroup) {

      chat.user.lastactivegroup = chat.user.activegroup;
      chat.user.activegroup = null;

    };

  });

  $(window).focus(function () {

    chat.setactivegroup(chat.user.lastactivegroup);

    chat.user.lastactivegroup = null;

    chat.focuscheckin();

  });

  //Click on notification

  $("body").on("click", ".chat-notification-header", function () {

    var message = $(this).closest(".chat-notification");

    $(message).fadeOut();

    var group = message.attr("data-groupid");

    chat.setactivegroup(group);

    if (!chat.page.popout) {

      Drupal.toggleSlidein();

      $('html, body').scrollTop($("#block-hub-chat-chat-window").offset().top - 80);
    } else {

      chat.popup.focus();

    };

  });

  // Mark agenda item complete
  $("#chat-panel").on("click", "#meeting-window .agenda-items input", function () {

    var itemid = $(this).attr('data-itemid');
    var entityref = $(this).parent().parent().attr('data-entityref');
    var checked = $(this).prop('checked');

    if (itemid && entityref) {

      if (checked) {
        chat.updateagendaitem('check', itemid, entityref);
      } else {
        chat.updateagendaitem('uncheck', itemid, entityref);
      }
    }

  });

  //Typing

  chat.events.typing = function (message) {

    $("#typing").text(message);

  };

  // Toggle the full page view
  chat.toggleExpanded = function (close) {
    if ($('#chat-panel').hasClass('expanded') || close == true) {
      $('#chat-panel').removeClass('expanded');
      $('#chat-panel .mediacall-window-surround').removeClass('col-sm-9');
      $('#chat-panel #conversation-wrapper').removeClass('col-sm-3');
    } else {
      $('#chat-panel').addClass('expanded');
      $('#chat-panel .mediacall-window-surround').addClass('col-sm-9');
      $('#chat-panel #conversation-wrapper').addClass('col-sm-3');
    }
  }

  Drupal.behaviors.mymodule = {
    attach: function (context, settings) {

      if ($('#hub-settings', context).length > 0 && $('#hub-settings:not(.ajax-processed)').length > 0) {
        var link = $('#hub-settings');
        var element_settings = {};
        // Clicked links look better with the throbber than the progress bar.
        element_settings.progress = {
          'type': 'throbber'
        };

        // For anchor tags, these will go to the target of the anchor rather
        // than the usual location.
        if (link.attr('href')) {
          element_settings.url = link.attr('href');
          element_settings.event = 'click';
        }
        var base = link.attr('id');
        var myAjax = Drupal.ajax[base] = new Drupal.ajax(base, link, element_settings);

        myAjax.options.success = function (response, status) {
          //Trigger Attach Behaviors
          setTimeout(function () {
            Drupal.attachBehaviors($(myAjax.selector))
          }, 0);
          // Sanity check for browser support (object expected).
          // When using iFrame uploads, responses must be returned as a string.
          if (typeof response == 'string') {
            response = $.parseJSON(response);
          }
          myAjax.options.beforeSend = function (response, status) {
            $('#chat-settings').toggleClass('closed');
            return false;
          }
          return myAjax.success(response, status);
        }

        link.addClass("ajax-processed"); //add class when we're done

      }

      window.setInterval(function () {
        $('.event-start').each(function (index, element) {

          $(this).html('(' + Drupal.theme.prototype.MeetingTime($(this).attr('data-starttime') * 1000, $(this).attr('data-endtime') * 1000) + ')');

        });
      }, 1000);

    }
  }

  chat.timeline = function (prepend) {

    var populateTimeline = function () {
      //Sort the posts by time

      chat.posts.sort(function (a, b) {

        if (a.time < b.time)
          return -1;
        if (a.time > b.time)
          return 1;
        return 0;

      });

      var knownMessages = {};

      $.each(chat.posts, function (index, message) {

        if (message.group.isReadOnly && message.message.userid == 1) {

          // This is a hack.
//          if (!knownMessages[message.message._id]) {
            $('#chat-timeline').prepend(Drupal.theme.prototype.TimelinePost(message));
//          }

//          knownMessages[message.message._id] = true;
        }

      });
    };

    $('#chat-timeline').html("");

    if (!prepend) {

      var groupcount = 0;

      //Loop over each group to get the count of them. Oh objects pre ECMASCRIPT 5.

      $.each(chat.groups, function (number, element) {

        groupcount += 1;

      });

      //Create a timeline object to store the posts in

      chat.posts = [];

      //Loop over each group to get its messages

      //Keep track of which group we're on so we know which group we're on.

      var count = 0;

      $.each(chat.groups, function (groupid, group) {

        chat.fetchgroupmessages(groupid, function () {

          $.each(group.messages, function (count, message) {

            chat.posts.push({

              group: group,
              message: message,
              time: chat.mongotime(message._id)

            });

          });

          count += 1;

          if (count === groupcount) {

            populateTimeline();

          };



        });



      });

    } else {
      populateTimeline();
    }



  };

  chat.timelinepush = function (message) {

    if ($('#chat-timeline').length) {
      var post = {

        group: chat.groups[message.groupid],
        message: message,
        time: chat.mongotime(message._id)

      }

      chat.posts.push(post);

      chat.timeline(true);

    }

  }

  chat.events.ready = function () {
    if ($('#chat-timeline').length) {
      window.setTimeout(function() {
        chat.timeline();
      }, 500);
    }
  }

}(jQuery));
